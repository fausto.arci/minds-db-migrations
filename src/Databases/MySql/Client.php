<?php
declare(strict_types=1);

namespace Minds\MindsDbMigration\Databases\MySql;

use Exception;
use PDO;

class Client
{
    private const INITIALISATION_SQL_FILE = __DIR__ . '/init.sql';
    private const MIGRATIONS_FOLDER = __DIR__ . '/migrations';

    private readonly PDO $dbConnection;

    public function __construct()
    {
        $this->dbConnection = new PDO(
            sprintf(
                'mysql:host=%s;port=%s',
                $_ENV['DB_HOST'],
                $_ENV['DB_PORT']
            ),
            $_ENV['DB_USER'],
            $_ENV['DB_PASSWORD']
        );
    }

    /**
     * @return void
     * @throws Exception
     */
    public function initialise(): void
    {
        $this->processInitialisationSql();
        $this->processMigrations();
    }

    /**
     * @return array
     */
    private function getMigrations(): array
    {
        return array_diff(scandir(self::MIGRATIONS_FOLDER), ['.', '..']);
    }

    /**
     * @return void
     */
    public function processMigrations(): void
    {
        $this->dbConnection->exec('USE ' . $_ENV['DB_NAME'] . ';');
        $migrations = $this->getMigrations();
        foreach ($migrations as $migration) {
            $this->processMigration($migration);
        }
    }

    /**
     * @return void
     */
    private function processInitialisationSql(): void
    {
        $initialisationSql = file_get_contents(self::INITIALISATION_SQL_FILE);
        $this->dbConnection->exec($initialisationSql);

        // TODO: handle errors
    }

    /**
     * @param string $migrationFilename
     * @return void
     */
    private function processMigration(string $migrationFilename): void
    {
        $migration = file_get_contents(self::MIGRATIONS_FOLDER . '/' . $migrationFilename);
        $this->dbConnection->exec($migration);

        // TODO: handle errors
    }
}