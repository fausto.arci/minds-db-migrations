FROM php:8.2-fpm-alpine3.18

RUN docker-php-ext-install -j$(nproc) pdo pdo_mysql

### Installing cassandra extension
# RUN export BUILD_DEPS=$'\
#     cmake \
#     make \
#     pcre-dev \
#     libuv-dev \
#     git \
#     gmp-dev \
#     libtool \
#     openssl-dev \
#     libstdc++ \
#     zlib-dev' \
#     && export INSTALL_DIR=/usr/src/datastax-php-driver \
#     # Cassandra extension \
#     && apk add --no-cache --virtual build-deps $BUILD_DEPS $PHPIZE_DEPS \
#     && apk add --no-cache libuv gmp libstdc++ \
#     && git clone --branch=v1.3.x https://github.com/he4rt/scylladb-php-driver.git $INSTALL_DIR \
#     && cd $INSTALL_DIR \
#     && git checkout 7f871a5be0a21d22cf7754b6b0281ab0b0c92999 \
#     && git submodule update --init \
#     # Install CPP Driver
#     && cd $INSTALL_DIR/lib/cpp-driver \
#     && mkdir build && cd build \
#     && cmake -DCASS_BUILD_STATIC=ON -DCASS_BUILD_SHARED=ON .. \
#     && make && make install \
#     # Install PHP Driver
#     && cd $INSTALL_DIR/ext \
#     && phpize && ./configure && make && make install \
#     && docker-php-ext-enable cassandra \
#     && apk del build-deps \
#     && rm -rf $INSTALL_DIR

WORKDIR /var/www/minds-db-migration