# Minds DBs' migrations tool

This tool is used to apply changes to Minds databases across environments

## 1. Minds Database types

Currently supported Minds DB types are:

- Vitess (MySQL)
- Cassandra
- OpenSearch

## 2. Migration files

Migration files are located in the `migrations` folder of each database type

### 2.1 Naming convention

Every migration file **MUST** be named in the following format:

`<unix_timestamp>_<minds_project>_<ticket_number>.<db_dialect_extension>`

Example:

`1623345600_minds_1234.sql`